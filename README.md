# README #

### test43943 React Front-End Interview Test

Please read the pdf
for the full description with screenshots

#### Introduction

In this test your task is to display a list of music albums and songs, using React and Redux.

#### Specification

* Test data: in the data.json file we provide a static list of songs.
* Each song in the list has 3 attributes: band, album, song
* We expect to see a list of bands and albums with in an expandable/collapsible panel for the
  songs within the given album (see screenshots on page 2):
    * The expand/collapse button on the individual albums should expand/collapse the list of songs
    * The expand/collapse button on the main Albums title should expand/collapse all the albums
* The main technologies you are expected to use
    * Choice of ajax: to get the list of songs from the server
    * Redux: to store the data
    * React: to display the list
* The supported browsers are: IE11, Firefox, Chrome, the solution expected to work on these.
  (note: only IE11, no backward compatibility with earlier IE is expected)

The following files are included in the zip file:

|  File                            |  Description                                 |
|  ------------------------------  |  ------------------------------------------  |        
|  public/data.json                |  Static dataset for the test in json format  |
|  public/index.html               |  Main html page                              |
|  src/                            |  Empty source directory                      |
|  package.json                    |  Basic template for npm / yarn|              |
|  yarn.lock                       |  Empty template for yarn                     |
|  test43943 React Front-end [...] |  This file                                   |


#### Expected solution

1. The main purpose of this test is for you to show your ability to design and implement
   bespoke web sites using React/Redux.  
   We expect a working implementation, not just a screen design.
2. Please use ajax for getting the list of songs from the server, do not just import them.
3. Please use Redux to store the data.
4. Please use React for displaying the list.
5. You're free to use any related library you choose, this test not just about implementation skills, but  
   to see your design choices and your ability to create an integrated solution from multiple technologies.


#### Screenshots of example screens

![Screen Shot 2017-04-03 at 22.40.04.png](https://bitbucket.org/repo/5qxr5oo/images/37536905-Screen%20Shot%202017-04-03%20at%2022.40.04.png)

![Screen Shot 2017-04-03 at 22.40.50.png](https://bitbucket.org/repo/5qxr5oo/images/398891032-Screen%20Shot%202017-04-03%20at%2022.40.50.png)

*(Youâ€™re free to use any iconset you like, these are here just an example for expand/collapse icons)*


## Live Server
Yon see the app up and running live from here: http://ec2-54-154-35-216.eu-west-1.compute.amazonaws.com:8084/




## How to install the App on your local laptop

### With Docker:
1. Install the docker client for your machine if not installed previously. Instructions: https://docs.docker.com/install/
2. Copy the Dockerfile (https://bitbucket.org/txabox/test43943/src) in your desired folder.
3. Open a terminal on that folder and build the test43943 image running this code (this might take a while):
```
docker build -t test43943_image .
```
4. Run a container out from the previous image running this code:
```
docker run -p 8084:8084 --rm -i --name test43943_container test43943_image
```
5. You can access the web going to http://localhost:8084/ on your browser
6. To stop the container running you have to open a new terminal and run this code:
```
docker stop test43943_container
```
7. And finally run this code to remove all the used images:
```
docker rmi test43943_image ubuntu
```

### Without Docker:
1. Install nvm if is not installed on your computer. Instructions: https://github.com/creationix/nvm
2. Set the node version to v9.3.0 in nvm. Instructions: https://www.sitepoint.com/quick-tip-multiple-versions-node-nvm/
3. Install pm2 globally if is not installed with:
```
npm install pm2 -g
```
4. Install git if not installed. Instructions here: https://git-scm.com/book/en/v2/Getting-Started-Installing-Git
5. Clone the repository to the desired folder: https://bitbucket.org/txabox/test43943
6. Install all the dependencies with:
```
npm install
```
7. Package all the files with webpack like this (choose whether you want the App to be run in 'dev' or 'prod' environment):
```
npm run start:front:[dev|prod]
```
8. Run the express server (running in port 8084) (choose whether you want the App to be run in 'dev' or 'prod' environment):
```
npm run start:back:[dev|prod]
```
9. You can access the web going to http://localhost:8084/ on your browser
