import express from 'express'
import axios from 'axios'

const router = express.Router()
router
  .get('/songs', (req, res) => {
    axios.get(`http://${req.headers.host}/assets/data/data.json`)
      .then((response) => {
        res.status(response.status).json(response.data)
      })
      .catch((error) => {
        console.error('Error retrieving the json: ', error)
        res.status(error.response.status).json(error.response.data)
      })
  })

export default router
