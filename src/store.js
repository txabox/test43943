import {
  createStore,
  combineReducers
} from 'redux'

import index from './reducers/index/indexReducer'
import albums from './reducers/albums/albumsReducer'

const reducers = combineReducers({
  index,
  albums
})

export default createStore(reducers)
