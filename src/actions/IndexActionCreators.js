export const storeData = (songs) => {
  const albums = []

  const getAlbum = (name, band) => albums.find(album => album.name === name && album.band === band)

  songs.map((song, i) => {
    const curAlbum = getAlbum(song.album, song.band)
    if (!curAlbum) {
      // Insert a new album with this 1st song
      albums.push({
        id: `album_${i}`,
        name: song.album,
        band: song.band,
        songs: [{
          id: `song_${i}`,
          name: song.song
        }],
        isShown: false
      })
    } else {
      // the album already existed. Insert the new song
      curAlbum.songs.push({
        id: `song_${i}`,
        name: song.song
      })
    }
  })

  albums.sort((a, b) => a.band > b.band ? 1 : -1)

  return {
    type: 'STORE_ALBUMS_DATA',
    payload: {
      albums
    }
  }
}
