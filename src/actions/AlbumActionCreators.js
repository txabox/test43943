export const setIsShown = (id, isShown) => {
  return {
    type: 'ALBUM_SET_IS_SHOWN',
    payload: {
      id,
      isShown
    }
  }
}
