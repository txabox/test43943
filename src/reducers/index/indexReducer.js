const defaultValues = {
  isRetrievingData: true
}

const indexReducer = (state = defaultValues, action) => {
  const newState = {...state}

  if (action.type === 'STORE_ALBUMS_DATA') {
    newState.isRetrievingData = false
  }
  return newState
}

export default indexReducer
