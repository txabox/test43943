const defaultValues = {
  array: []
}

const indexReducer = (state = defaultValues, action) => {
  const newState = {...state}

  if (action.type === 'STORE_ALBUMS_DATA') {
    newState.array = action.payload.albums
  } else if (action.type === 'ALBUM_SET_IS_SHOWN') {
    // immutably deep copy of the array
    const newArray = JSON.parse(JSON.stringify(newState.array))
    // find the affected album
    const album = newArray.find((el) => el.id === action.payload.id)
    // set its property
    album.isShown = action.payload.isShown
    // store the utterly new immutable array
    newState.array = newArray
  }
  return newState
}

export default indexReducer
