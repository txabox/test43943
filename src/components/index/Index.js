import React from 'react'
import PropTypes from 'prop-types'
import axios from 'axios'
import styled from 'styled-components'

import LoadingSpinner from './components/LoadingSpinner/LoadingSpinner'
import Albums from './components/Albums/Albums'

import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

import * as IndexActionCreators from 'Actions/IndexActionCreators'

const StyledIndex = styled.div`
  height: 100%;
`

class Index extends React.Component {
  componentDidMount() {
    axios.get('/api/songs')
      .then((response) => {
        const songs = response.data

        this.props.actions.storeData(songs)
      })
      .catch((error) => {
        console.error('Error retrieving songs. Error: ', error)
        alert('There has been an error retrieving songs from the server. Have a look at the console for more info')
      })
  }

  render() {
    return <StyledIndex>
      {this.props.isRetrievingData
        ? <LoadingSpinner />
        : <Albums />
      }
    </StyledIndex>
  }
}

Index.propTypes = {
  isRetrievingData: PropTypes.bool.isRequired,
  actions: PropTypes.object.isRequired
}

const mapStateToProps = (state) => {
  return {
    isRetrievingData: state.index.isRetrievingData
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    actions: bindActionCreators(IndexActionCreators, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Index)
