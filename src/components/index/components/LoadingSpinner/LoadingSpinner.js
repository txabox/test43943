import React, { Component } from 'react'
import { ScaleLoader } from 'halogenium'

import styled from 'styled-components'

const NAVBAR_H = 50

const StyledLoadingSpinner = styled.div`
.loading {
  box-sizing: border-box;
  display: flex;
  flex: 0 1 auto;
  flex-direction: row;
  flex-wrap: wrap;

  .loading-internal {
    align-items: center;
    display: flex;
    flex: 0 1 auto;
    flex-basis: 100%;
    flex-direction: column;
    flex-grow: 1;
    flex-shrink: 0;
    height: ${props => props.height - NAVBAR_H}px;
    justify-content: center;
    max-width: 100%;
  }
}
`

class LoadingSpinner extends Component {
  constructor(props) {
    super(props)
    this.state = { height: 600 }
  }

  componentDidMount() {
    this.updateWindowDimensions()
    window.addEventListener('resize', this.updateWindowDimensions)
  }

  /**
   * Remove event listener
   */
  componentWillUnmount() {
    window.removeEventListener('resize', this.updateWindowDimensions)
  }

  updateWindowDimensions = () => {
    const height = this.$el.parentNode.clientHeight
    if (height !== this.state.height) {
      this.setState({
        height
      })
    }
  }

  shouldComponentUpdate(nextProps, nextState) {
    if (nextState.height !== this.state.height) {
      return true
    }
    return false
  }

  render() {
    return (
      <StyledLoadingSpinner
        innerRef={div => (this.$el = div)}
        height={this.state.height}>
        <div className="loading">
          <div className="loading-internal">
            <ScaleLoader color='#666'/>
          </div>
        </div>
      </StyledLoadingSpinner>
    )
  }
}

export default LoadingSpinner
