import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

import { connect } from 'react-redux'

import Album from './components/Album/Album'

const StyledAlbums = styled.div`
  h1{
    margin-bottom: 25px;
  }
`

const Albums = ({albums}) => <StyledAlbums>
  <h1>Albums:</h1>
  {
    albums.map((album) => {
      return <Album
        key={album.id}
        album={album} />
    })

  }
</StyledAlbums>

Albums.propTypes = {
  albums: PropTypes.array.isRequired
}

const mapStateToProps = (state) => {
  return {
    albums: state.albums.array
  }
}

export default connect(mapStateToProps)(Albums)
