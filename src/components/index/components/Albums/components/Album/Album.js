import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { Panel, Glyphicon } from 'react-bootstrap'

import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

import * as AlbumActionCreators from 'Actions/AlbumActionCreators'

import Songs from './components/Songs/Songs'

const StyledAlbum = styled.div`
  .panel-heading{
    cursor: pointer;
  }
`

class Album extends React.Component {
  handleClick = () => {
    const {actions, album} = this.props
    actions.setIsShown(album.id, !album.isShown)
  }

  render() {
    const {album} = this.props
    return <StyledAlbum>
      <Panel bsStyle="info">
        <Panel.Heading onClick={this.handleClick}>
          <Panel.Title componentClass="h3">

            <span className='pull-right'>
              <Glyphicon glyph={album.isShown ? 'chevron-up' : 'chevron-down'} />
            </span>

            {album.band} - {album.name}
          </Panel.Title>
        </Panel.Heading>
        {
          album.isShown
          ? <Songs songs={album.songs} />
          : null
        }
      </Panel>
    </StyledAlbum>
  }
}

Album.propTypes = {
  album: PropTypes.object.isRequired,
  actions: PropTypes.object.isRequired
}

const mapDispatchToProps = (dispatch) => {
  return {
    actions: bindActionCreators(AlbumActionCreators, dispatch)
  }
}

export default connect(null, mapDispatchToProps)(Album)
