import React from 'react'
import PropTypes from 'prop-types'
import { ListGroupItem } from 'react-bootstrap'

const Song = ({song}) => <ListGroupItem>{song.name}</ListGroupItem>

Song.propTypes = {
  song: PropTypes.object.isRequired
}

export default Song
