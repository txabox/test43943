import React from 'react'
import PropTypes from 'prop-types'

import { ListGroup } from 'react-bootstrap'

import Song from './components/Song/Song'

const Songs = ({songs}) => <ListGroup>
  {
    songs.map(song => <Song
      key={song.id}
      song={song} />)
  }
</ListGroup>

Songs.propTypes = {
  songs: PropTypes.array.isRequired
}

export default Songs
